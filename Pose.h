#ifndef _POSE_H_
#define _POSE_H_
#include<iostream>
#include<fstream>
using namespace std;
class Pose {

private:
	//!@param x is the position of the robot on x-axis
	float x = 0;
	//!@param y is the position of the robot on 7-axis
	float y = 0;
	//!@param th is the angle which the robot is going towards
	float th = 0;
	//!\brief operator<< to help the class Path
	friend ostream& operator<<(ostream&, Pose);
public:
	//!\brief getX to get the x value
	float getX();
	//!\brief setX to set the x value
	void setX(float);
	//!\brief getY to get the y value
	float getY();
	//!\brief setY to set the y value
	void setY(float);
	//!\brief getTh to get the th value
	float getTh();
	//!\brief setTh to set the th value
	void setTh(float);
	//!\brief operator== to know if two poses are equal
	bool operator==(const Pose&);
	//!\brief operator!= to know if two poses are not equal
	bool operator!=(const Pose&);
	//!\brief operator+ to add two poses
	Pose operator+(const Pose&);
	//!\brief operator- to get the difference of two poses
	Pose operator-(const Pose&);
	//!\brief operator+= to add another pose to a pose
	Pose& operator+=(const Pose&);
	//!\brief operator-= to substract another pose from a pose
	Pose& operator-=(const Pose&);
	//!\brief operator< to know if a pose is smaller than another one
	bool operator<(const Pose&);
	//!\brief getPose to get a pose, with all three values (x, y, th)
	void getPose(float& _x, float& _y, float& _th);
	//!\brief setPose to set a pose, with all three values (x, y, th)
	void setPose(float _x, float _y, float _th);
	//!\brief findDistanceTo to find the distance between two poses
	float findDistanceTo(Pose pos);
	//!\brief findAngleTo to find the angle between two poses
	float findAngleTo(Pose pos);


};
#endif