#include "PioneerRobotAPI.h"
#include "SonarSensor.h"
#include <iostream>
using namespace std;

PioneerRobotAPI* robot;
float sonars[16];

void print() {
	SonarSensor sonar(robot);
	cout << "MyPose is (" << robot->getX() << "," << robot->getY() << "," << robot->getTh() << ")" << endl;
	cout << "Sonar ranges are [ ";
	sonar.updateSensor(sonars);
	for (int i = 0; i < 16; i++) {
		cout << sonars[i] << " ";
	}
	cout << "]" << endl;
	int index = 0;
	cout << "Max Range: " << sonar.getMax(index) << "\t" << "Sonar index of Max Range: " << index << endl;
	cout << "Min Range: " << sonar.getMin(index) << "\t" << "Sonar index of Min Range: " << index << endl;
	cout << "Range of SonarSensor[" << index << "]: " << sonar.getRange(index) << endl;
	cout << "Angle of SonarSensor[" << index << "]: " << sonar.getAngle(index) << endl;
}

int main() {

	robot = new PioneerRobotAPI;

	if (!robot->connect()) {
		cout << "Could not connect..." << endl;
		return 0;
	}

	robot->moveRobot(300);
	print();

	robot->turnRobot(PioneerRobotAPI::DIRECTION::left);
	Sleep(1000);
	print();

	robot->turnRobot(PioneerRobotAPI::DIRECTION::forward);
	Sleep(1000);
	print();

	robot->turnRobot(PioneerRobotAPI::DIRECTION::right);
	Sleep(1000);
	print();

	robot->stopRobot();
	robot->setPose(100, 200, 30);
	Sleep(1000);
	print();

	cout << "Press any key to exit...";
	getchar();

	robot->disconnect();
	delete robot;
	return 0;

}