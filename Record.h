#ifndef RECORD_H
#define RECORD_H
#include <iostream>
#include <fstream>
#include <string>
using namespace std;
//! It is a file i/o operation class.
/*! This class writes anything user wants to any file as text output. */
class Record {
private:
	string fileName;/*!< It is name of file which will be read from or write to. */
	fstream file;/*!< It is the file which will be used to write or read */
public:
	Record() { fileName = "data.txt"; }/*!< Default constructor function. */
	Record(string name) { fileName = name; }/*!< Constructor function with input. */
	bool openFile();/*!< It opens file which will be read or written. */
	bool closeFile();/*!< It close file which will be read or written. */
	void setFileName(string);/*!< This function set file's name. */
	string readLine();/*!< This function read a line from file. */
	bool writeLine(string);/*!< This function writes a line to file. */
	void operator>>(string);/*!< This operator reads data from a file and prints it on console screen. */
};

#endif