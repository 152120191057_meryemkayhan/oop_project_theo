/*
Author:Or�un Balatl�o�lu
ID:152120181065
Date:09.01.2021
*/

#include <iostream>
#include "Path.h"
using namespace std;

void main(void) {
	Pose c1,c2,c3,c4,c5,c6,c7,c9;
	c1.setPose(0.1, 0.2, 0.3);
	c2.setPose(0.2, 0.3, 0.4);
	c3.setPose(0.3, 0.4, 0.5);
	c4.setPose(0.4, 0.5, 0.6);
	c5.setPose(0.5, 0.6, 0.7);
	c6.setPose(0.6, 0.7, 0.8);
	c7.setPose(0.7, 0.8, 0.9);
	Path p1,p2;
	
	p1.addPos(c1);
	p1.addPos(c2);
	p1.addPos(c3);
	p2.addPos(c4);
	p2.insertPos(0,c5);
	p2.addPos(c6);
	cout << "----Before Operations----" << endl;
	p1.print();
	p2.print();
	cout << "Removing p1[2]..." << endl;
	cout << "Inserting c7 to p1[1]..." << endl<<endl;
	p1.removePos(2);
	p1.insertPos(0, c7);
	cout << "----After Operations----" << endl;
	p1.print();
	p2.print();
	
	Pose c8 = p1.getPos(2);
	cout <<"c8: "<< c8 << endl;
	cout << "Adding c8 to end of p1..." << endl << endl;
	p1.addPos(c8);
	c9 >> p2;
	cout << "Adding pose have entered to end of p2..." << endl << endl;
	cout << "----After Operations----" << endl;
	p1.print();
	p2.print();
	cout <<"p1[0]: "<< p1[0]->pose << endl;
}