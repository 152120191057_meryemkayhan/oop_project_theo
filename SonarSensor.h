/**
* @file SonarSensor.h
* @Author Emin Cing�z (emincingoz@gmail.com)
* @date January, 2021
* @brief SonarSensor Class definiton and definitions of necessary functions was made.
*
*/

#pragma once
#include "RangeSensor.h"
using namespace std;

#define numofSensor 16

class SonarSensor : public RangeSensor
{
	const float angles[numofSensor] = { 90, 50, 30, 10, -10, -30, -50, -90, 90, 50, 30, 10, -10, -30, -50, -90 };
public:
	//!	Constructor
	SonarSensor(PioneerRobotAPI* robot);
	//! \brief setRange function dynamically creates an array an initialize it with 0.
	void setRange();
	//!	getRange function it returns the range of sensor with the given index value
	float getRange(int index) const;
	//!	getMax function returns the max range of sonar sensors and sets the index value of the sonar sensor with max range to index variable
	float getMax(int& index) const;
	//!	getMin function returns the min range of sonar sensors and sets the index value of the sonar sensor with min range to index variable
	float getMin(int& index) const;
	//!	updateSensor function assigns the values in the array given as parameters to private array of sonar sensor ranges
	void updateSensor(float* _ranges);
	//!	getAngle function returns the angle of sensor at the given index 
	float getAngle(int index) const;
};