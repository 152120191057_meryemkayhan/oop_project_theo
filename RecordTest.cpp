/*
Author:Or�un Balatl�o�lu
ID:152120181065
Date:09.01.2021
*/

#include "Record.h"
#include<iostream>

using namespace std;

void main(void) {
	string name;
	int point = 0;
	Record file = Record();
	cout << "Enter your file name: ";
	cin >> name;
	cout << endl;
	file.setFileName(name);
	if (file.openFile()) {
		if (file.writeLine("Hello,This code written by Or�un Balatl�o�lu.\n"))
			point += 5;
		if (file.writeLine("This code is written for OOP lesson."))
			point += 5;
		string temp = file.readLine();
		cout << temp << endl;
		temp = file.readLine();
		if (file.writeLine("This code is written."))
			point += 5;
		if (file.writeLine("This code is written for OOP."))
			point += 5;
		temp = file.readLine();//do not continue from last read location starting again from begining
		cout << temp << endl;
	}
	else{
		cout << "File can't be opened." << endl;
		exit(0);
	}
	cout << point;

}