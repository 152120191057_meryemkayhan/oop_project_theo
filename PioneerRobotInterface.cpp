#include "PioneerRobotInterface.h"
#include "SonarSensor.h"
#include "LaserSensor.h"
#include "RangeSensor.h"
using namespace std;

/**
 * @param robot is a parameter that type of PioneerRobotAPI*
 */
PioneerRobotInterface::PioneerRobotInterface(PioneerRobotAPI* robot)
{
	robotAPI = robot;
	position.setPose(robotAPI->getX(), robotAPI->getY(), robotAPI->getTh());
	counter = 0;
	sensors.push_back(new SonarSensor(robotAPI));
	sensors.push_back(new LaserSensor(robotAPI));
}
void PioneerRobotInterface::turnLeft()
{
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::left);
}
void PioneerRobotInterface::turnRight()
{
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::right);
}
/**
 * @param speed is a float parameter to adjust the amount of speed
 */
void PioneerRobotInterface::forward(float speed)
{
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::forward);
	robotAPI->moveRobot(speed);
}
/**
 * @param speed is a float parameter to adjust the amount of speed
 */
void PioneerRobotInterface::backward(float speed)
{
	robotAPI->moveRobot((-1) * speed);
}
void PioneerRobotInterface::stopTurn()
{
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::forward);
}
void PioneerRobotInterface::stopMove()
{
	robotAPI->stopRobot();
}
/**
 * @param pose is a Pose& type parameter takes the robot's current location as a parameter
 */
void PioneerRobotInterface::setPose(const Pose& pose)
{
	Pose newpose;
	newpose.setPose(robotAPI->getX(), robotAPI->getY(), robotAPI->getTh());
	Pose temp = pose;
	position = newpose - temp;
}
/**
 * \return The Pose information of robot from the RobotControl class
 */
Pose PioneerRobotInterface::getPose() {
	if (counter > 0)
		setPose(position);

	counter++;
	return position;
}
void PioneerRobotInterface::print()
{
	Pose pose = getPose();
	cout << "MyPose is (" << pose.getX() << "," << pose.getY() << "," << pose.getTh() << ")" << endl;
	updateSensors();
}
void PioneerRobotInterface::updateSensors()
{

	float Sranges[16], Lranges[1000];
	sensors[0]->updateSensor(Sranges);
	sensors[1]->updateSensor(Lranges);
}
vector<RangeSensor*> PioneerRobotInterface::getSensors() { return sensors; }