/**
* @file RangeSensor.h
* @Author Emin Cing�z (emincingoz@gmail.com)
* @date January, 2021
* @brief RangeSensor Abstract Class definiton and definitions of necessary functions was made.
*
*/

#pragma once
#include "PioneerRobotAPI.h"
using namespace std;

class RangeSensor
{
protected:
	//!	@param ranges used as an array, the size is assigned in child classes.
	float* ranges;
	//!	@param robotAPI it is a pointer created from the PioneerRobotAPI.
	PioneerRobotAPI* robotAPI;

public:
	//!	Constructor
	RangeSensor(PioneerRobotAPI* robot);
	//!	\brief getRange function is a pure virtual function in the RangeSensor Class
	//! @param index integer parameter to take index information
	virtual float getRange(int index) const = 0;
	//!	\brief getMax is pure virtual function in RangeSensor Class
	//! @param index is a integer parameter with &
	virtual float getMax(int& index) const = 0;
	//! \brief Pure virtual getMin function returns the min of the sensor ranges
	//! @param index is a integer parameter with & to hold index information of the sensor with the min range.
	virtual float getMin(int& index) const = 0;
	//! \brief updateSensor is a pure virtual function and it update the sensors then record them to the dynamically created ranges array.
	//! @param ranges is the array from which to receive the update.
	virtual void updateSensor(float* ranges) = 0;
	//! \brief operator[] function allows us to access the ranges values of sensors with the [] operator.
	//! @param i is used to get index.
	float operator[](int i);
	//! \brief getAngle function returns the angle information of the sensor with entered index value.
	//!  @param index is a integer parameter to get the index of sensor.
	virtual float getAngle(int index) const = 0;
};