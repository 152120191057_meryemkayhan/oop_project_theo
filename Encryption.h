/**
* @file Encryption.h
* @Author Ay�e Rabia Soylu (ayserabiasoylu@gmail.com)
* @date December, 2020
* @brief Encryption Class definiton and definitions of necessary functions are made.
*
* Detailed description of file.
*/

#ifndef ENCRYPTION_H
#define ENCRYPTION_H
#include <string>
using namespace std;
class Encryption {
public:
	/// <summary>
	/// Encrypts 4-digit numbers and check whether the password has 4 digit or not. 
	/// </summary>
	/// <param name="code"> the data to be encrypted </param>
	/// <returns> encrypted data </returns>
	int encrypt(int code);
	/// <summary>
	/// Decrypts 4-digit numbers.
	/// </summary>
	/// <param name="secret"></param>
	/// <returns>the data to be decrypted</returns>
	int decrypt(int secret);
};
#endif // !ENCRYPTION_H
