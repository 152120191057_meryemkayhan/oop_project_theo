#include "RobotOperator.h"
#include <iostream>
using namespace std;

int main(void) {

	RobotOperator admin("isaac", "asimov", 7658);

	admin.checkAccessCode(7658);
	admin.print();

	RobotOperator anonimous;
	anonimous.checkAccessCode(0000);
	anonimous.print();

	system("pause");
	return 0;
}