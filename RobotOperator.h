/**
* @file RobotOperator.h
* @Author Ay�e Rabia Soylu (ayserabiasoylu@gmail.com)
* @date December, 2020
* @brief RobotOperator Class definiton and definitions of necessary functions are made.
*
* Detailed description of file.
*/
#ifndef ROBOTOPERATOR_H
#define ROBOTOPERATOR_H
#include <string>
#include "Encryption.h"
using namespace std;
class RobotOperator: public Encryption {
private:
	//! name of the operator has access permission.
	string name;
	//! surname of the operator has access permission.
	string surname;
	//! the password to be used by the operator has access permission.
	unsigned int accessCode;
	//! access state of the operator. If it is 0, it means the operator did not enter right password.
	bool accessState;
	/// <summary>
	/// Encrypts 4-digit numbers and check whether the password has 4 digit or not. 
	/// </summary>
	/// <param name="code"> the data to be encrypted </param>
	/// <returns> encrypted data </returns>
	int encryptCode(int secret);
	/// <summary>
	/// Decrypts 4-digit numbers.
	/// </summary>
	/// <param name="secret"></param>
	/// <returns>the data to be decrypted</returns>
	int decryptCode(int password);
public:
	//! Constructor
	RobotOperator();
	//! Parametered Constructor
	RobotOperator(string name, string surname, int accessCode);
	/// <summary>
	/// It checks if the entered code is the same as the accessCode, which is kept encrypted.
	/// 0 means the password entered is not the same with the accessCode.
	/// 1 means everything is okay.
	/// </summary>
	/// <param name="password"></param>
	/// <returns> whether the password is true or not, which is entered by the operator. </returns>
	bool checkAccessCode(int password);
	/// <summary>
	/// Displays the operator's name and access status.
	/// </summary>
	void print();
};
#endif // !ROBOTOPERATOR_H
