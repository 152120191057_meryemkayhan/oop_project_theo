/**
* @file Path.h
* @Author Or�un Balatl�o�lu 152120181065
* @date 09/01/2021
* @brief This file keeps Path class' definitions.
*/

#ifndef PATH_H
#define PATH_H
#include <iostream>
#include <ostream>
#include "Node.h"
using namespace std;
//! An storage class
/*! This class created to store coordinates of the robot in a list and acces them easily. */
class Path : public Pose {
private:
	friend void operator>>(Pose, Path&);/*!< The function is for overloading ">>" operator to put any coordinates to the end of list easily.*/
	Node* tail;/*!< The pointer keeps last coordinate in the list so it doesn't lose any coordinate.*/
	Node* head;/*!< The pointer keeps first coordinate in the list so it doesn't lose any coordinate.*/
	int number;/*!< @param number keeps there is how many coordinate in list. */
public:
	Path() { number = 0; tail = head = NULL; }/*!< Constructor of class. */
	//bool clearPath();
	bool addPos(Pose);/*!< The function adds the given coordinate to end of the list. */
	void print();/*!< The function prints all coordinates to screen in the list. */
	Node* operator [] (int);/*!< This operator overloading is created to acces any coordinate in the list quickly and easily. */
	Pose getPos(int);/*!< It is for accesing coordinates in nodes. */
	bool removePos(int);/*!< The function is removing the node in given index. */
	bool insertPos(int, Pose);/*!< It is adding a node as next node of given index's node. */
	int getNumOfPose();/*!< This is an support function that returns number of class if necessary.*/
};
#endif
