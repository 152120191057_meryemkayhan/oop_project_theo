#include "RobotControl.h"
#include "PioneerRobotInterface.h"
#include "PioneerRobotAPI.h"
#include <iostream>
#include <string>
using namespace std;


//Path* path = new Path;
//Record* record = new Record;
//RobotOperator* operator0 = new RobotOperator("ziya", "selcuk", 1234);

PioneerRobotAPI* robotAPI= new PioneerRobotAPI;
//PioneerRobotInterface* PioRobotInter = new PioneerRobotInterface(robotAPI);

int main()
{
	
	//RobotControl* robot = new RobotControl(PioRobotInter, path, record, operator0);
	RobotControl* robot = new RobotControl(new PioneerRobotInterface(robotAPI), new Path, new Record, new RobotOperator("ziya", "selcuk", 1234));
	
	if (!robotAPI->connect()) {
		cout << "Could not connect..." << endl;
		exit(0);
	}

	robot->openAccess(1234);

	robot->closeAccess(1234);

	robot->openAccess(1234);

	robot->forward(1000);
	robot->print();
	Sleep(1000);
	robot->stopMove();
	robot->print();
	robot->addToPath();

	robot->turnLeft();
	Sleep(1000);
	robot->print();
	robot->addToPath();

	robot->turnLeft();
	Sleep(1000);
	robot->print();
	robot->addToPath();

	robot->forward(500);
	Sleep(5000);
	robot->print();
	robot->addToPath();

	robot->stopMove();
	Sleep(1000);
	robot->print();
	robot->addToPath();

	robot->turnRight();
	Sleep(1000);
	robot->print();
	robot->addToPath();

	robot->backward(600);
	robot->turnRight();
	Sleep(1000);
	robot->print();
	robot->addToPath();

	robot->stopMove();
	Sleep(1000);
	robot->print();
	robot->addToPath();

	robot->forward(600);
	robot->turnRight();
	Sleep(1000);
	robot->print();
	robot->addToPath();

	robot->recordPathToFile();
	robot->clearPath();
	
	robot->forward(700);
	Sleep(1000);
	robot->stopMove();
	robot->addToPath();

	robot->turnLeft();
	Sleep(1000);
	robot->print();
	robot->addToPath();

	robot->recordPathToFile();
	robot->stopMove();

	robotAPI->disconnect();

	cout << "Press any key to exit...";
	getchar();
	
	

	system("pause");
	return 0;
}