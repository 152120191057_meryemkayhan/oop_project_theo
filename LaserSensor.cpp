#include "LaserSensor.h"
#include <iostream>
using namespace std;

LaserSensor::LaserSensor(PioneerRobotAPI* robot) : RangeSensor(robot) { setRange(); }

void LaserSensor::setRange() { ranges = new float[numofSensor] (); }

void LaserSensor::updateSensor(float* _ranges) {
	robotAPI->getLaserRange(_ranges);
	for (int i = 0; i < numofSensor; i++)
	{
		ranges[i] = _ranges[i];
	}
}

float LaserSensor::getMin(int& index) const {
	float min = ranges[0];
	for (int i = 0; i < 181; i++) {
		if (ranges[i] < min) {
			min = ranges[i];
			index = i;
		}
	}
	return min;
}

float LaserSensor::getMax(int& index) const {
	float max = ranges[0];
	for (int i = 0; i < 181; i++) {
		if (ranges[i] > max) {
			max = ranges[i];
			index = i;
		}
	}
	return max;
}

float LaserSensor::getRange(int index) const {
	if (index >= 0 && index <= 180) return ranges[index];
	else {
		cout << "There is no laser sensor at specified index.  " << endl;
		exit(0);
	}
}

float LaserSensor::getAngle(int index) const { return index; }

float LaserSensor::getClosestRange(float startAngle, float endAngle, float& angle) {
	float closest = this->ranges[(int)startAngle];
	for (int i = (int)startAngle; i <= (int)endAngle; i++) {
		if (this->ranges[i] < closest) {
			closest = this->ranges[i];
			angle = i;
		}
	}
	return closest;
}