#include <iostream>
#include "RangeSensor.h"
using namespace std;

RangeSensor::RangeSensor(PioneerRobotAPI* robot) { 
	this->robotAPI = robot; 
	
}

/**
 *	@param i is an integer parameter
 *	\return	Range information of index i of the sensors
 */
float RangeSensor::operator[](int i)
{
	return getRange(i);
}