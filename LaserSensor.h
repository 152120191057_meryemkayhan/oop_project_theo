﻿/**
* @file LaserSensor.h
* @Author Emin Cingöz (emincingoz@gmail.com)
* @date January, 2021
* @brief LaserSensor Class definiton and definitions of necessary functions was made.
*
*/

#pragma once
#include "RangeSensor.h"
using namespace std;

#define numofSensor 181

class LaserSensor : public RangeSensor {
public:
	//! Constructor
	//! @param robot is the robot which sensor is on 
	LaserSensor(PioneerRobotAPI* robot);
	//! \brief setRange function dynamically creates an array an initialize it with 0.
	void setRange();
	/// <summary>
	/// returns the distance information of sensor at parametered index.
	/// </summary>
	/// <param name="index"> used to indicate index of sensor </param>
	/// <returns> distance information </returns>
	float getRange(int index) const;
	/// <summary>
	/// Uploads the robot's current sensor distance values ​​to the ranges array.
	/// </summary>
	/// <param name="ranges"> array will be filled sensor distance values. </param>
	/// <returns> distance information </returns>
	void updateSensor(float* _ranges);
	/// <summary>
	/// Detect the minimum of the distance values. 
	/// Index of data with minimum distance copies the referenced parameter.
	/// </summary>
	/// <param name="index"></param>
	/// <returns> the minimum of the distance values </returns>
	float getMax(int& index) const;
	/// <summary>
	/// Detect the maximum of the distance values. 
	/// Index of data with maximum distance copies the referenced parameter.
	/// </summary>
	/// <param name="index"></param>
	/// <returns> the maximum of the distance values </returns>
	float getMin(int& index) const;
	/// <summary>
	/// Returns the angle value of the sensor whose index is given.
	/// </summary>
	/// <param name="index"></param>
	/// <returns>angle value</returns>
	float getAngle(int index) const;
	/// <summary>
	/// The angle of the smallest of the distances between startAngle and endAngle angles is copied the referenced parameter angle.
	/// </summary>
	/// <param name="startAngle"></param>
	/// <param name="endAngle"></param>
	/// <param name="angle"></param>
	/// <returns> the smallest of the distances between startAngle and endAngle angles </returns>
	float getClosestRange(float startAngle, float endAngle, float& angle);
};