#ifndef PIONEER_ROBOT_INTERFACE
#define PIONEER_ROBOT_INTERFACE

#include <iostream>
#include "RobotInterface.h"
#include "PioneerRobotAPI.h"
#include "Pose.h"
using namespace std;

class PioneerRobotInterface : public RobotInterface {

	//!	@param position is a Pose type object to operate with the robot's position information
	Pose position;
	//!	@param robotAPI is an object of type PioneerRobotAPI* and it is used to retrieve the current data from the PioneerRobotAPI class
	PioneerRobotAPI* robotAPI;
	//!	@param counter is an integer variable that counts the number of poses
	int counter;
public:
	//!	Constructor
	PioneerRobotInterface(PioneerRobotAPI*);
	//!	turnLeft function allows the rotot to turn left
	void turnLeft();
	//!	turnRight function allows the robot to turn right
	void turnRight();
	//!	forward function is used for the robot to move forward
	void forward(float speed);
	//!	print function prints the information of pose to the output screen
	void print();
	//!	backward function allows the robot to move back 
	void backward(float speed);
	//!	getPose is a function that returns the robot's pose information
	Pose getPose();
	//!	setPose assigns the robot's current pose information to the Pose object defined in this class
	void setPose(const Pose& pose);
	//!	stopTurn stops the turning operation of robot
	void stopTurn();
	//!	stopMove function interrupts the robot's motion
	void stopMove();
	//!	UpdateSensors function updates the robot's sensors
	void updateSensors();
	//!	getSensors vector helps us reach robot's sensor infornations easily
	vector<RangeSensor*> getSensors();
};





#endif
