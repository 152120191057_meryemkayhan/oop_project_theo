/**
* @file Menu.h
* @Author Emin Cing�z (emincingoz@gmail.com)
* @date December, 2020
* @brief Menu Class definiton and definitions of necessary functions was made.
*
*/

#include <iostream>
#include <vector>
#include "PioneerRobotAPI.h"
#include "Pose.h"
#include "RobotControl.h"
#include "SonarSensor.h"
#include "Record.h"
#include "LaserSensor.h"
#include "RobotOperator.h"
#include "RangeSensor.h"
#include "PioneerRobotInterface.h"
using namespace std;

class Menu :public PioneerRobotAPI//, public Record, public RobotOperator
{
	PioneerRobotAPI* robotAPI;
	PioneerRobotInterface* robotInterface;
	Record* record;
	Path* path;
	RobotOperator* robotOperator;
	RobotControl* robot;
	string name;
	string surname;
	int password;
	bool operatorFLAG;
	
public:
	//!	Constructor
	Menu(PioneerRobotAPI*);
	//!	MainMenu function created for the main menu operations
	void MainMenu();
	//!	Connection is a function that provides connection to the robot
	void Connection();
	//!	Motion function provides the Motion of the robot
	void Motion();
	//!	Recordings function writes to file and reads from the file 
	void Recording();
	//! RobotOperatorMenu is used to adding new operator, log in and accessing information of operator
	void RobotOperatorMenu();
	//! Information is used to print information abot robot
	void Information();
};