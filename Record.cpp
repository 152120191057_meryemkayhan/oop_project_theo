/*
Author:Or�un Balatl�o�lu
ID:152120181065
Date:09.01.2021
*/

#include <iostream>
#include "Record.h"

using namespace std;

/*!
	\return true or false on depending situation.
*/
bool Record::openFile() {
	file.open(fileName, ios::out);
	if (file.is_open())
		return true;
	else
		return false;
}
/*!
	\return true or false on depending situation.
*/
bool Record::closeFile() {
	file.close();
	if (file.is_open())
		return false;
	else
		return true;
}
/*!
	\param name is name of will be set.
*/
void Record::setFileName(string name) {
	fileName = name;
}
/*!
	\return The text is read from file.
*/
string Record::readLine() {
	file.close();
	file.open(fileName, ios::in);
	string s;
	getline(file, s, '\n');
	return s;
}
/*!
	\param s is text will be written to file.
	\return true or false on depending situation.
*/
bool Record::writeLine(string s) {
	file.close();
	file.open(fileName, ios::app | ios::out);
	if (file.is_open()) {
		if (s[s.size() - 1] != '\n') file << s << endl;
		else file << s;
		return true;
	}
	else
		return false;
}
/*!
	\param fname is file's name that will be used.
*/
void Record::operator>>(string fname) {
	fstream fl;
	cout << fname << endl;
	fl.open(fname, ios::in);
	if (fl.is_open())
	{
		string s;
		getline(fl, s, '\n');
		cout << s << endl;
		while (!fl.eof())
		{
			getline(fl, s, '\n');
			cout << s << endl;
		}
	}
}