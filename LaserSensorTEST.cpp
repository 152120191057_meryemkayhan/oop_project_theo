#include "PioneerRobotAPI.h"
#include "LaserSensor.h"
#include <iostream>
using namespace std;

PioneerRobotAPI* robot;
float ranges[181];

void print() {
	LaserSensor Laser(robot);
	cout << "MyPose is (" << robot->getX() << "," << robot->getY() << "," << robot->getTh() << ")" << endl;
	cout << "Laser ranges are [ ";
	Laser.updateSensor(ranges);
	for (int i = 0; i < 181; i++) {
		cout << ranges[i] << " ";
	}
	cout << "]" << endl;
	int index = 0;
	cout << "Max Range: " << Laser.getMax(index) << "\t" << "Laser index of Max Range: " << index << endl;
	cout << "Min Range: " << Laser.getMin(index) << "\t" << "Laser index of Min Range: " << index << endl;
	cout << "Range of Laser Sensor[" << index << "]: " << Laser.getRange(index) << endl;
	cout << "Angle of Laser Sensor[" << index << "]: " << Laser.getAngle(index) << endl;
}

int main() {

	robot = new PioneerRobotAPI;

	if (!robot->connect()) {
		cout << "Could not connect..." << endl;
		return 0;
	}

	robot->moveRobot(100);
	print();

	robot->turnRobot(PioneerRobotAPI::DIRECTION::left);
	Sleep(1000);
	print();

	robot->turnRobot(PioneerRobotAPI::DIRECTION::forward);
	Sleep(1000);
	print();

	robot->turnRobot(PioneerRobotAPI::DIRECTION::right);
	Sleep(1000);
	print();

	robot->stopRobot();
	robot->setPose(100, 200, 30);
	Sleep(1000);
	print();

	cout << "Press any key to exit...";
	getchar();

	robot->disconnect();
	delete robot;
	return 0;

}