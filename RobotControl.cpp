#include "RobotControl.h"
#include <string>
using namespace std;


RobotControl::RobotControl( RobotInterface* robotinter,Path* path, Record* record, RobotOperator* operator_)
{
	this->robotInter = robotinter;
	this->accessState = false;
	this->record = record;
	this->path = path;
	this->operator0 = operator_; 
	this->position = robotInter->getPose();
}

RobotControl::~RobotControl() {
	sensors.clear();
	delete path;
	delete record;
	delete operator0;
	delete robotInter;
}

bool RobotControl::readLineRobot(string name) {
	record->setFileName(name);
	string s = record->readLine();
	if (s == "" || s == " " || s == "\n")
		return false;
	else {
		cout << s << endl;
		return true;
	}
}

void RobotControl::setOperator(RobotOperator* oper) {
	this->operator0 = oper;
}

bool RobotControl::getAccessState() {
	return this->accessState;
}

bool RobotControl::clearPath()
{
	if (this->accessState == false) return false;
	while (path->getNumOfPose() > 0) {
		path->removePos(0);
	}
	if (path->getNumOfPose() == 0) return true;
	else return false;
}

bool RobotControl::recordPathToFile()
{
	if (this->accessState == false) return false;
	bool status_writing = false;
	string name;
	cout << "Enter your file's name with extention(.txt,.bin ....): ";
	cin >> name;
	record->setFileName(name);
	cin.ignore();
	system("cls");
	for (int i = 0; i < path->getNumOfPose(); i++) {
		string poseInfo;
		poseInfo = "Pose[" + to_string(i) + "]: " + " ( " + to_string(path->getPos(i).getX()) +
			", " + to_string(path->getPos(i).getY()) +
			"," + to_string(path->getPos(i).getTh()) + " )";
		status_writing = record->writeLine(poseInfo);
	}
	return status_writing;
}

bool RobotControl::addToPath()
{
	if (this->accessState == false) return false;
	this->position = robotInter->getPose();
	return path->addPos(position);
	
}

void RobotControl::openAccess(int password)
{
	this->accessState = operator0->checkAccessCode(password);
	if(this->accessState == true) cout << "ACCESS GRANTED." << endl;
	else cout << "Incorrect Password..." << endl;

}

void RobotControl::closeAccess(int password)
{
	bool x;
	x = operator0->checkAccessCode(password);
	if (x == true)
	{
		this->accessState = false;
		cout << "ACCESS DENIED." << endl;
	}
	else {
		cout << "Incorrect Password..." << endl;
		this->accessState = true;
	}
	
}

void RobotControl::turnLeft()
{
	if (this->accessState == false) return;
	robotInter->turnLeft();
}

void RobotControl::turnRight()
{
	if (this->accessState == false) return;
	robotInter->turnRight();
}

/**
 * @param speed is a float parameter to adjust the amount of speed 
 */
void RobotControl::forward(float speed)
{
	if (this->accessState == false) return;
	robotInter->forward(speed);
}

/**
 * @param speed is a float parameter to adjust the amount of speed
 */
void RobotControl::backward(float speed)
{
	if (this->accessState == false) return;
	robotInter->backward(speed);
}

void RobotControl::stopTurn()
{
	if (this->accessState == false) return;
	robotInter->stopTurn();
}

void RobotControl::stopMove()
{
	if (this->accessState == false) return;
	robotInter->stopMove();
}

/**
 * @param pose is a Pose& type parameter takes the robot's current location as a parameter
 */
void RobotControl::setPose(const Pose& pose)
{
	if (this->accessState == false) return;
	robotInter->setPose(pose);
}

/**
 * \return The Pose information of robot from the RobotControl class
 */
Pose RobotControl::getPose() { 
	return robotInter->getPose();
}

void RobotControl::print()
{
	if (this->accessState == false) return;
	robotInter->print();
	sensorEqualize(robotInter->getSensors());
	//updateSensors();

	cout << "Sonar ranges are [ ";
	//sonar.updateSensor(sonars);
	for (int i = 0; i < 16; i++) {
		cout << sensors[0]->getRange(i) << " ";
	}
	cout << "]" << endl;
	int index = 0;
	cout << "Max Range: " << sensors[0]->getMax(index) << "\t" << "Sonar index of Max Range: " << index << endl;
	cout << "Min Range: " << sensors[0]->getMin(index) << "\t" << "Sonar index of Min Range: " << index << endl;
	cout << "Range of SonarSensor[" << index << "]: " << sensors[0]->getRange(index) << endl;
	cout << "Angle of SonarSensor[" << index << "]: " << sensors[0]->getAngle(index) << endl;

	cout << "Laser ranges are [ ";
	//Laser.updateSensor(ranges);
	for (int i = 0; i < 181; i++) {
		cout << sensors[1]->getRange(i) << " ";
	}
	cout << "]" << endl;
	index = 0;
	cout << "Max Range: " << sensors[1]->getMax(index) << "\t" << "Laser index of Max Range: " << index << endl;
	cout << "Min Range: " << sensors[1]->getMin(index) << "\t" << "Laser index of Min Range: " << index << endl;
	cout << "Range of Laser Sensor[" << index << "]: " << sensors[1]->getRange(index) << endl;
	cout << "Angle of Laser Sensor[" << index << "]: " << sensors[1]->getAngle(index) << endl;
}

void RobotControl::sensorEqualize(vector<RangeSensor*>_sensors) { sensors = _sensors; }