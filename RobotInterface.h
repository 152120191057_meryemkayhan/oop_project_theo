/**
* @file RobotInterface.h
* @Author Emin Cing�z (emincingoz@gmail.com)
* @date January, 2021
* @brief RobotInterface Abstract Class definiton and definitions of necessary functions was made.
*
*/

#pragma once
#include "Pose.h"
#include "RangeSensor.h"
#include "SonarSensor.h"
#include "LaserSensor.h"
#include<vector>
using namespace std;

class RobotInterface {
protected:
	//!	@param position It is a attribute type of Pose*
	Pose* position;
	//!	@param state is a int variable
	int state;
	//! @param sensors is a vector in type RangeSensor*
	vector<RangeSensor*>sensors;
public:
	//! \brief turnLeft pure virtual function used to turn the robot to the left.
	virtual void turnLeft() = 0;
	//! \brief pure virtual turnRight function used to turn the robot to the right.
	virtual void turnRight() = 0;
	//! \brief forward function allows the robot move forward at a given speed as parameter
	//! @param speed is a float parameter to take the speed
	virtual void forward(float speed) = 0;
	//! \brief backward function allows the robot move backward at the given speed.
	//! //! @param speed is a float parameter to take the speed
	virtual void backward(float speed) = 0;
	//! \brief print Pose information of robot 
	virtual void print() = 0;
	//! \brief getPose function returns the pose information of the robot
	virtual Pose getPose() = 0;
	//! \brief the setPose function transfers the pose information, which it takes as a parameter, to the private pose attribute.
	virtual void setPose(const Pose& pose) = 0;
	//! \brief stopTurn stops the robot's turn
	virtual void stopTurn() = 0;
	//! \brief stopMove stops the robot's move
	virtual void stopMove() = 0;
	//! \brief updateSensors updates the sensor informations and records them to the vector in type RangeSensor*
	virtual void updateSensors() = 0;
	//! \brief getSensors function returns the vector created at PioneerRobotInterface class
	virtual vector<RangeSensor*> getSensors() = 0;
};