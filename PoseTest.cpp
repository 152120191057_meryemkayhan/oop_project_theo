#include<iostream>
#include "Pose.h"
using namespace std;
void PoseTest();

int main()
{
	PoseTest();

	cout << endl << endl;
	system("pause");
	return 0;
}
void PoseTest()
{
	Pose p1, p2;
	Pose p3, p4, p5, p6, p7, p8;

	cout << "Setting and getting poses" << endl;
	p1.setX(5);  p1.setY(12);   p1.setTh(67.4);
	cout << "p1  x = " << p1.getX() << "  y = " << p1.getY() << "  th = " << p1.getTh() << endl;

	p2.setX(3);  p2.setY(4);   p2.setTh(53.1);
	cout << "p2  x = " << p2.getX() << "  y = " << p2.getY() << "  th = " << p2.getTh() << endl;

	cout << "\nOperator == " << endl;
	p5.setX(5);  p5.setY(12);   p5.setTh(67.4);
	cout << "p5   x = " << p5.getX() << "  y = " << p5.getY() << "  th = " << p5.getTh() << endl;
	if (p5 == p1)
		cout << "p5 and p1 equal poses" << endl;
	if (p5 == p2)
		cout << "p5 and p2 equal poses" << endl;
	else
		cout << "p5 and p2 not equal poses" << endl;

	cout << "\nOperator + " << endl;
	p3 = p1 + p2;
	cout << "p1+p2=p3   x = " << p3.getX() << "  y = " << p3.getY() << "  th = " << p3.getTh() << endl;

	cout << "\nOperator - " << endl;
	p4 = p2 - p1;
	cout << "p1-p2=p4   x = " << p4.getX() << "  y = " << p4.getY() << "  th = " << p4.getTh() << endl;

	cout << "\nOperator += " << endl;
	p6 = p1;	p6 += p2;
	cout << "p6+=p2   x = " << p6.getX() << "  y = " << p6.getY() << "  th = " << p6.getTh() << endl;

	cout << "\nOperator -= " << endl;
	p7 = p2;	p7 -= p1;
	cout << "p7-=p1   x = " << p7.getX() << "  y = " << p7.getY() << "  th = " << p7.getTh() << endl;

	cout << "\nOperator < " << endl;
	if (p1 < p2)
		cout << "operator <  p1 is smaller than p2" << endl;
	else
		cout << "operator <  p1 is not smaller than p2" << endl;

	cout << "\nFinding angle and distance " << endl;
	cout << "findAngleTo    the angle between p2 and p1 is " << p2.findAngleTo(p1) << endl;
	cout << "findDistanceTo   the distance between p2 and p1 is " << p2.findDistanceTo(p1) << endl;

	cout << "\nSetting and gettting pose " << endl;
	float a = 0, b = 0, c = 0;
	p2.setPose(5, 12, 67.4);
	p2.getPose(a, b, c);
	cout << "new pose p2   x = " << a << " y = " << b << " th = " << c << endl;
}