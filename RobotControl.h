/**
* @file RobotControl.h
* @Author Ay�e Rabia Soylu
* @date January, 2021
* @brief RobotControl Class definiton and the definitions of necessary functions was made.
*
*/

#ifndef ROBOTCONTROL_H
#define ROBOTCONTROL_H
#include <iostream>
#include "RobotInterface.h"
#include "RangeSensor.h"
#include "Path.h"
#include "Record.h"
#include "RobotOperator.h"
#include "Pose.h"
#include <vector>
using namespace std;

class RobotControl
{
private:
	Pose position;
	Path* path;
	Record* record;
	RobotOperator* operator0;
	bool accessState;
protected:
	RobotInterface* robotInter;
	vector<RangeSensor*> sensors;
public:
	//!	Constructor
	RobotControl(RobotInterface*, Path*, Record*, RobotOperator*);
	//!	Destrcutor
	~RobotControl();
	//!	turnLeft function allows the rotot to turn left
	void turnLeft();
	//!	turnRight function allows the robot to turn right
	void turnRight();
	//!	forward function is used for the robot to move forward
	void forward(float speed);
	//!	print function prints the information of pose to the output screen
	void print();
	//!	backward function allows the robot to move back 
	void backward(float speed);
	//!	getPose is a function that returns the robot's pose information
	Pose getPose();
	//!	setPose assigns the robot's current pose information to the Pose object defined in this class
	void setPose(const Pose& pose);
	//!	stopTurn stops the turning operation of robot
	void stopTurn();
	//!	stopMove function interrupts the robot's motion
	void stopMove();
	/// <summary>
	/// Adds current position of the robot to the path.
	/// </summary>
	/// <returns> If the robot's position as same as previous position, returns false </returns>
	bool addToPath();
	/// <summary>
	/// removes the all positions adding to the path.
	/// </summary>
	/// <returns>returns true if the path was cleared successfully.</returns>
	bool clearPath();
	/// <summary>
	/// the positions adding to the path is written to a file.
	/// </summary>
	/// <returns> if poses are written successfully to the file returns true. </returns>
	bool recordPathToFile();
	/// <summary>
	/// used to give permission of access to the operator with him password.
	/// </summary>
	/// <param name="password">operator's access code</param>
	void openAccess(int password);
	/// <summary>
	/// used to revoke permission of access to the operator with him password.
	/// </summary>
	/// <param name="password">operator's access code</param>
	void closeAccess(int password);
	/// <summary>
	/// sets the registered robot by using Robot Operator Menu.
	/// </summary>
	/// <param name="oper"> operator which controls robot </param>
	void setOperator(RobotOperator* oper);
	/// <summary>
	/// current data of the sensors is synchronized
	/// </summary>
	/// <param name="_sensors">laser and sonar sensors </param>
	void sensorEqualize(vector<RangeSensor*>_sensors);
	/// <summary>
	/// reads data from the file.
	/// </summary>
	/// <param name="name"></param>
	/// <returns>whether if the reading process occured successfully or not </returns>
	bool readLineRobot(string name);
	/// <summary>
	/// returns the access state of the operator.
	/// </summary>
	bool getAccessState();
};
#endif // !ROBOTCONTROL_H

