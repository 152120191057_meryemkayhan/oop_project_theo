#include "Menu.h"
#include <conio.h>
#include <string>
using namespace std;

/**
 *	@param robot is a parameter of type PioneerRobotAPI*
 */
Menu::Menu(PioneerRobotAPI* robot) 
{ 
	robotAPI = robot;
	robotInterface = new PioneerRobotInterface(robotAPI);
	record = new Record;
	//sensors = new 
	path = new Path;
	robotOperator = new RobotOperator;
	this->operatorFLAG = false;
	this->robot = new RobotControl(robotInterface, path, record, robotOperator);
	
}

void Menu::MainMenu()
{
	char answer;
	do
	{
		cout << endl << "Main Menu" << endl;
		cout << "1. Connection" << endl;
		cout << "2. Motion" << endl;
		cout << "3. Information" << endl;
		cout << "4. Recording" << endl;
		cout << "5. Robot Operator" << endl;
		cout << "6. Quit" << endl;
		
		cout << endl << "Choose One: ";

		answer = _getch();
		system("cls");
		//cout << "answer: " << answer << endl;
		
		/*if (answer == 49)
			AccessMenu();*/
		if (answer == 49)
			Connection();
		else if (answer == 50)
			Motion();
		else if (answer == 51)
			Information();
		else if (answer == 52)
			Recording();	//cout << "record" << endl;	//Recording();
		else if (answer == 53)
			RobotOperatorMenu();
		else if (answer == 54)
			exit(0);

	} while (answer != 54);
}

void Menu::Connection()
{
	if (operatorFLAG == false)
	{
		cout << "No registered operator. Register first please." << endl;
		return;
	}
	else if (robot->getAccessState() == false)
	{
		cout << "No permission of access. Open access by using Robot Operator Menu." << endl;
		return;
	}

	char answer;
	do
	{
		cout << endl << "Connection Menu" << endl;
		cout << "1. Connect Robot" << endl;
		cout << "2. Disconnect Robot" << endl;
		cout << "3. Back" << endl;

		cout << endl << "Choose One: ";

		answer = _getch();
		system("cls");

		if (answer == 49)
		{
			if (!robotAPI->connect())
			{
				cout << "Could not connect..." << endl;
				exit(0);
			}
			else	cout << "<Connect> " << endl << "Robot is Connected..." << endl;
			MainMenu();
		}
		else if (answer == 50)
		{
			robotAPI->disconnect();
			cout << "<Disconnect> " << endl << "Robot is disconnected..." << endl;
			exit(0);
		}
		else if (answer == 51)
			MainMenu();

	} while (answer != 51);
}

void Menu::Motion()
{
	if (operatorFLAG == false)
	{
		cout << "No registered operator. Register first please." << endl;
		return;
	}
	else if (robot->getAccessState() == false)
	{
		cout << "No permission of access. Open access by using Robot Operator Menu." << endl;
		return;
	}

	char answer;
	do
	{
		cout << endl << "Motion Menu" << endl;
		cout << "1. Move Robot(forward)" << endl;
		cout << "2. Move Robot(backward)" << endl;
		cout << "3. Turn Left" << endl;
		cout << "4. Turn Right" << endl;
		cout << "5. Stop Move" << endl;
		cout << "6. Stop Turn" << endl;
		cout << "7. Back" << endl;
		cout << "8. Quit" << endl;

		cout << endl << "Choose One: ";

		answer = _getch();
		system("cls");


		if (answer == 49)
		{
			robot->forward(500);
			robot->addToPath();
			Sleep(1000);
		}
		else if (answer == 50)
		{
			robot->backward(500);
			robot->addToPath();
			Sleep(1000);
		}
		else if (answer == 51)
		{
			robot->turnLeft();
			robot->addToPath();
			Sleep(1000);
		}
		else if (answer == 52)
		{
			robot->turnRight();
			robot->addToPath();
			Sleep(1000);
		}
		else if (answer == 53)
			robot->stopMove();
		else if (answer == 54)
			robot->stopTurn();
		else if (answer == 55)
			MainMenu();
		else if (answer == 56)
			exit(0);
	} while (answer != 56);
}

void Menu::Information()
{
	if (operatorFLAG == false)
	{
		cout << "No registered operator. Register first please." << endl;
		return;
	}
	else if (robot->getAccessState() == false)
	{
		cout << "No permission of access. Open access by using Robot Operator Menu." << endl;
		return;
	}

	char answer;
	do
	{
		cout << endl << "Information Menu" << endl;
		cout << "1. Print Information" << endl;
		cout << "2. Back" << endl;

		cout << endl << "Choose One: ";

		answer = _getch();
		system("cls");

		if (answer == 49)
		{
			robot->print();
		}
		else if (answer == 50)
		{
			MainMenu();
		}

	} while (answer != 50);
}

void Menu::Recording() {
	if (operatorFLAG == false)
	{
		cout << "No registered operator. Register first please." << endl;
		return;
	}
	else if (robot->getAccessState() == false)
	{
		cout << "No permission of access. Open access by using Robot Operator Menu." << endl;
		return;
	}


	char answer;
	do {
		cout << endl << "Record Menu" << endl;
		cout << "1.Write Path" << endl;
		cout << "2.Read Line" << endl;
		cout << "3.Add Pose to Path" << endl;
		cout << "4.Clear Path" << endl;
		cout << "5.Back" << endl;
		cout << endl << "Choose one: " << endl;

		answer = _getch();
		system("cls");

		if (answer == 49)
		{
			if (robot->recordPathToFile()) {
				cout << "Your input line is written to file succesfuly.";
				MainMenu();
			}
			else {
				cout << "Your input line couldn't be written.Check your file's name and input.Then try again.";
				MainMenu();
			}
		}
		else if (answer == 51)
		{
			robot->addToPath();
		}
		else if (answer == 52)
		{
			robot->clearPath();
		}
		else if (answer == 50)
		{
			string name;
			bool status;
			cout << "Enter your file's name with extention(.txt,.bin ....): ";
			cin >> name;
			cin.ignore();
			system("cls");

			status = robot->readLineRobot(name);
			if (!status)
				cout << "Your file couldn't be read.File is empty." << endl;
			else
				cout << endl << "Press any key to continue...";
			MainMenu();
		}

		else if (answer == 53)
			MainMenu();

	} while (answer != 53);

}

void Menu::RobotOperatorMenu() {
	char answer;
	do
	{
		cout << endl << "Robot Operator Menu" << endl;
		cout << "1. Add An Operator" << endl;
		cout << "2. Open Access" << endl;
		cout << "3. Close Access" << endl;
		cout << "4. Back" << endl;

		cout << endl << "Choose One: ";

		answer = _getch();
		system("cls");

		if (answer == 49)
		{
			cout << "Enter your name: ";
			getline(cin, this->name);
			cout << "Enter your surname: ";
			getline(cin, this->surname);
			cout << "Enter your 4 digit password: ";
			cin >> this->password;
			RobotOperator robo_oper(name, surname, password);
			robot->setOperator(&robo_oper);
			robotOperator = &robo_oper;
			this->operatorFLAG = 1;
			cin.ignore();
			system("cls");
		}
	
		else if (answer == 50) {
			if (this->operatorFLAG == 0) {
				cout << "There is no registered operator. Please register first.";
				//system("cls");
			}
			else if (this->operatorFLAG == 1) {
				cout << "Enter your name: ";
				getline(cin, this->name);
				cout << "Enter your surname: ";
				getline(cin, this->surname);
				int temp;
				cout << "Enter your 4 digit password: ";
				cin >> temp;
				robot->openAccess(temp);
				cin.ignore();
			}

		}
		else if (answer == 51) {
			if (this->operatorFLAG == 0) {
				cout << "There is no registered operator. Please register first.";
				//system("cls");
			}
			else if (this->operatorFLAG == 1) {
				cout << "Enter your name: ";
				getline(cin, this->name);
				cout << "Enter your surname: ";
				getline(cin, this->surname);
				int temp;
				cout << "Enter your 4 digit password: ";
				cin >> temp;
				robot->closeAccess(temp);
				cin.ignore();
			}
			
		}
		else if (answer == 52)
		{
			MainMenu();
		}

	} while (answer != 52);
}
