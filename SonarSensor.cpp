#include <iostream>
#include "SonarSensor.h"
#include "RangeSensor.h"
using namespace std;

/**
 * @param robot is a parameter of type PioneerRobotAPI*
 */
SonarSensor::SonarSensor(PioneerRobotAPI* robot) : RangeSensor(robot) { setRange(); } //{ robotAPI = robot; }

void SonarSensor::setRange() { ranges = new float[numofSensor] (); }

/**
 *	@param index an integer parameter to get the index information
 *	\return The range information of given index of sonar sensors
 **/
float SonarSensor::getRange(int index) const {
	if (index <= 15 && index >= 0)
		return ranges[index];
	else
	{
		cout << "SonarSensor::getRange(int index): No sonar senor information found in the specific index." << endl;
		exit(0);
	}
}

/**
 *	@param index is an int& type parameter to assign new values to the index variable
 *	\return The max range of sonar sensors
 **/
float SonarSensor::getMax(int& index) const
{
	float max = ranges[0];
	for (int i = 0; i < numofSensor; i++)
	{
		if (ranges[i] > max)
		{
			max = ranges[i];
			index = i;
		}
	}
	return max;
}

/**
 *	@param index is an int& type parameter to assign new values to the index variable
 *	\return The min range of sonar sensors
 */
float SonarSensor::getMin(int& index) const
{
	float min = ranges[0];
	for (int i = 0; i < numofSensor; i++)
	{
		if (ranges[i] < min)
		{
			min = ranges[i];
			index = i;
		}
	}
	return min;
}

/**
 *	@param ranges is a float array parameter to assign ranges of sonar sensors to private float array variable of class
 *
 */
void SonarSensor::updateSensor(float* _ranges)
{
	robotAPI->getSonarRange(_ranges);
	for (int i = 0; i < numofSensor; i++)
	{
		ranges[i] = _ranges[i];
	}
}

/**
 *	@param index is an integer parameter
 *	\return	The angle information of sonar sensor in given index value
 */
float SonarSensor::getAngle(int index) const
{
	return angles[index];
}