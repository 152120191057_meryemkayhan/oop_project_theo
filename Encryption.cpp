#include "Encryption.h"
#include <iostream>
#include <cmath>
using namespace std;
int Encryption::encrypt(int code) {
	try {
		int temp = code, j = 0;
		while (temp >= 10) {
			temp = temp / 10; 
			j++;
		}
		
		if (j != 3) throw(j+1);
	}
	
	catch (int x) {
		cout << "Access denied. The password must have 4 digits." << endl;
		cout << "The password you entered has " << x << " digits. - Try Again." << endl;
		exit(0);
	}
	
	int secret = 0;
	int digits[4];
	for (int i = 0; i < 4; i++) {
		digits[i] = ((code % 10)+ 7 ) % 10;
		if (i >= 2) {
			int temp = digits[i];
			digits[i] = digits[i - 2];
			digits[i - 2] = temp;
		}
		code = code / 10;
	}
	for (int i = 0; i < 4; i++) {
		secret = secret + digits[i] * (int)pow(10, i);
	}
	return secret;
 }

int Encryption::decrypt(int secret) {
	// �ifrelenmesi i�in girilen say� 4 basamakl�ysa ve onlar basama�� 0 ise burada ��z�me gelecek say� 3 basamakl� olabilir
	int code = 0;
	int digits[4];
	for (int i = 0; i < 4; i++) {
		digits[i] = secret % 10;
		if (i >= 2) {
			int temp = digits[i];
			digits[i] = digits[i - 2];
			digits[i - 2] = temp;
		}
		secret = secret / 10;
	}
	
	for (int i = 0; i < 4; i++) {
		if (digits[i] == 7)digits[i] = 0;
		else {
			digits[i] += 10;
			if (digits[i] - 7 > 10) digits[i] -= 17;
			else digits[i] -= 7;
		}
		code = code + digits[i] * (int)pow(10, i);
	}
	return code;
}