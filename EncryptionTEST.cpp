#include "Encryption.h"
#include <iostream>
#include <iomanip>

using namespace std;

int main(void) {

	Encryption AlanTuring;

	int key1 = 7658;
	cout <<setw(15)<<"secret: "<<key1 << setw(20) <<"coded secret: "<<AlanTuring.encrypt(key1)<<endl;
	int decodedKey1 = 2543;
	cout << setw(15) << "coded secret: " << decodedKey1 << setw(20) << "secret: " << AlanTuring.decrypt(decodedKey1) << endl;
	cout << endl;


	int key3 = 1651;
	cout << setw(15) << "secret: " << key3 << setw(20) << "coded secret: " << AlanTuring.encrypt(key3) << endl;
	int decodedKey3 = 2883;
	cout << setw(15) << "coded secret: " << decodedKey3 << setw(20) << "secret: " << AlanTuring.decrypt(decodedKey3) << endl;
	cout << endl;
	

	int key4 = 1650;
	cout << setw(15) << "secret: " << key4 << setw(20) << "coded secret: " << AlanTuring.encrypt(key4) << endl;
	int decodedKey4 = 2783;
	cout << setw(15) << "coded secret: " << decodedKey4 << setw(20) << "secret: " << AlanTuring.decrypt(decodedKey4) << endl;
	cout << endl;

	
	int key5 = 1050;
	cout << setw(15) << "secret: " << key5 << setw(20) << "coded secret: " << AlanTuring.encrypt(key5) << endl;
	int decodedKey5 = 2787;
	cout << setw(15) << "coded secret: " << decodedKey5 << setw(20) << "secret: " << AlanTuring.decrypt(decodedKey5) << endl;
	cout << endl;


	int key6 = 1700;
	cout << setw(15) << "secret: " << key6 << setw(20) << "coded secret: " << AlanTuring.encrypt(key6) << endl;
	int decodedKey6 = 7784;
	cout << setw(15) << "coded secret: " << decodedKey6 << setw(20) << "secret: " << AlanTuring.decrypt(decodedKey6) << endl;
	cout << endl;


	int key7 = 9109;
	cout << setw(15) << "secret: " << key7 << setw(20) << "coded secret: " << AlanTuring.encrypt(key7) << endl;
	int decodedKey7 = 7668;
	cout << setw(15) << "coded secret: " << decodedKey7 << setw(20) << "secret: " << AlanTuring.decrypt(decodedKey7) << endl;
	cout << endl;

	/*int key2 = 765;
	cout << setw(15) << "secret: " << key2 << setw(20) << "coded secret: " << AlanTuring.encrypt(key2) << endl;
	cout << endl;

	int decodedKey2 = 25435;
	cout << setw(15) << "coded secret: " << decodedKey2 << setw(20) << "secret: " << AlanTuring.decrypt(decodedKey2) << endl;
	cout << endl; */


	system("pause");
	return 0;
}