/**
* @file Node.h
* @Author Or�un Balatl�o�lu 152120181065
* @date 09/01/2021
* @brief This file keeps Node struct's definitions.
*/

#ifndef NODE_H
#define NODE_H
#include <iostream>
#include "Pose.h"

using namespace std;
//! It is a struct for keeping coordinates.
struct Node {
	Pose pose;/*!< Coordinates */
	Node* next;/*!< Pointer for accessing to next node. */
	Node(Pose pose) :pose(pose) { next = NULL; }/*!< Constructor function. */
	~Node() { next = NULL; }/*!< Destructor function. */
	
};
#endif