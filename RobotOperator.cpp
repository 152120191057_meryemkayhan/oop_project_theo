#include "RobotOperator.h"
#include "Encryption.h"
#include <iostream>
#include <string>
#include <iomanip>
using namespace std;

RobotOperator::RobotOperator(string name ="admin", string surname= "admin", int accessCode=1234):name(name), surname(surname){
	this->accessState = false;
	this->accessCode = encryptCode(accessCode);
} 

RobotOperator::RobotOperator() {
	this->name = "admin";
	this->surname = "admin";
	this->accessState = false;
	this->accessCode = encryptCode(1234);
}

int RobotOperator::encryptCode(int secret) {
	return encrypt(secret);
}

int RobotOperator::decryptCode(int password) {
	return decrypt(password);
}

bool RobotOperator::checkAccessCode(int password) {
	if (password == decryptCode(accessCode)) {
		this->accessState = true;
		return true;
	} 
	else {
		cout << "Incorrect Password"<<endl;
		this->accessState = false;
		return false;
	} 
}

void RobotOperator::print() {
	cout <<setw(30)<< "Operator Information"<<endl;
	cout << setw(15) << "Name: " << this->name<<endl;
	cout << setw(15) << "Surname: " << this->surname<<endl;
	cout << setw(15) << "Access State: ";
	if (this->accessState == false) cout << "No access permission found." << endl << endl;
	else cout << "Access permission is valid." << endl << endl;
}