/*
Author:Or�un Balatl�o�lu
ID:152120181065
Date:09.01.2021
*/

#include "Path.h"
#include <iostream>
#include <iomanip>

using namespace std;
/*!
	\param index is index of node which user want to acces.
	\return Node* in given index.
*/
Node* Path::operator[](int index) {
	int counter = 0;
	Node* temp = head;
	while (counter < number) {
		if (counter == index)
			return temp;
		if (temp->next != NULL)
			temp = temp->next;
		counter++;
	}
	throw std::exception("Path::operator[] The index is invalid.");
}
/*!
	\param out is reference of cout.
	\param pose is which pose user want to print to screen.
	\return Reference of cout for printing operation.
*/
ostream& operator<<(ostream& out, Pose pose) {
	out << "(" << pose.getX() << "," << pose.getY() << "," << pose.getTh() << ")";
	return out;
}
/*!
	\param pose is the coordinate which will be added to list.
	\param p1 is reference of pose list.
*/
void operator>>(Pose pose, Path& p1) {
	float x, y, th;
	cout << "Enter your coordinates:\n";
	cout << "x:";
	cin >> x;
	cout << "y:";
	cin >> y;
	cout << "th:";
	cin >> th;
	pose.setPose(x, y, th);
	p1.addPos(pose);
}
/*!
	\param pos is the coordinate which will be added to end of the list.
*/
bool Path::addPos(Pose pos) {
	Node* node = new Node(pos);
	if (number == 0) {
		tail = node;
		head = node;
		number++;
		return true;
	}
	else if (number == 1) {
		head->next = node;
		tail = node;
		number++;
		return true;
	}
	else {
		if (tail->pose != pos) {
			tail->next = node;
			tail = node;
			number++;
			return true;
		}
		else
			return false;
	}
	return false;
}



void Path::print() {
	int counter = 0;
	Node* temp = head;
	cout << "--------------------------------------" << endl;
	cout << "|   Index    |" << "       (x,y,th)        |" << endl;
	cout << "--------------------------------------" << endl;
	while (counter < number) {
		cout << "|  ";
		cout << "index[" << counter << "]" << setw(3) << "|";
		cout << fixed << setw(3) << setprecision(3) << "(" << temp->pose.getX() << "," << temp->pose.getY() << "," << temp->pose.getTh() << ")" << setw(2) << "  |" << endl;
		counter++;
		if (temp->next != NULL)
			temp = temp->next;

	}
	cout << "--------------------------------------\n\n";
}
/*!
	\param index is index of node which want to be accessed.
	\return Pose of node.
*/
Pose Path::getPos(int index) {
	Node* temp = head;
	int counter = 0;
	while (counter < number) {
		if (counter == index)
			return temp->pose;
		counter++;
		temp = temp->next;
	}
	throw std::exception("Path::getPos Index is not valid.");
}
/*!
	\param index is index of node which will be deleted.
	\return true or false depending on the situation.
*/
bool Path::removePos(int index) {
	int counter = 0;
	Node* temp = head;
	while (counter < number) {
		if (index == 0) {
			if (temp == head)
				head = head->next;
			number--;
			delete temp;
			return true;
		}
		else if (counter == number - 2 && index == number - 1) {
			tail = temp;
			temp = temp->next;
			number--;
			delete temp;
			return true;
		}
		else if (counter == index - 1) {
			Node* erase = temp->next;
			temp->next = temp->next->next;
			number--;
			delete erase;
			return true;
		}
		if (temp->next != NULL)
			temp = temp->next;
		counter++;
	}
	delete temp;
	return false;
}
/*!
	\param index is index of node where node will be add to next.
	\param The pose will be added to list.
	\return true or false depending on the situation.
*/
bool Path::insertPos(int index, Pose pose) {
	Node* temp = head;
	int counter = 0;
	while (counter < number) {
		if (counter == index) {
			Node* node = new Node(pose);
			if (number == counter + 1)
				addPos(node->pose);
			else {
				node->next = temp->next;
				temp->next = node;
				number++;
			}
			return true;
		}
		temp = temp->next;
		counter++;
	}
	return false;
}
/*!
	\return number of Path.
*/
int Path::getNumOfPose()
{
	return this->number;
}