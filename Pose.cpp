#include<iostream>
#include<math.h>
#include "Pose.h"
using namespace std;



float Pose::getX() { return x; }
void Pose::setX(float _x) { x = _x; }
float Pose::getY() { return y; }
void Pose::setY(float _y) { y = _y; }
float Pose::getTh() { return th; }
void Pose::setTh(float _th) { th = _th; }
bool Pose::operator==(const Pose& p1)
{
	if (this->x == p1.x && this->y == p1.y && this->th == p1.th)
		return true;
	else
		return false;
}
bool Pose::operator != (const Pose& p1) {
	if (this->x == p1.x && this->y == p1.y && this->th == p1.th) {
		return false;
	}
	else
		return true;
}
Pose Pose::operator+(const Pose& p1)
{
	Pose p2;
	float tmp = 0;
	p2.x = this->x + p1.x;
	p2.y = this->y + p1.y;
	tmp = atan(p2.y / p2.x);
	p2.th = tmp * (180 / 3.1415);
	return p2;
}
Pose Pose::operator-(const Pose& p1)
{
	Pose p2;
	float tmp = 0;
	p2.x = abs(this->x - p1.x);
	p2.y = abs(this->y - p1.y);
	tmp = atan(p2.y / p2.x);
	p2.th = tmp * (180 / 3.1415);
	return p2;
}
Pose& Pose::operator+=(const Pose& p1)
{
	float tmp = 0;
	this->x += p1.x;
	this->y += p1.y;
	tmp = atan(this->y / this->x);
	this->th = tmp * (180 / 3.1415);
	return *this;
}
Pose& Pose::operator-=(const Pose& p1)
{
	float tmp = 0;
	this->x = abs(this->x - p1.x);
	this->y = abs(this->y - p1.y);
	tmp = atan(this->y / this->x);
	this->th = tmp * (180 / 3.1415);
	return *this;
}
bool Pose::operator<(const Pose& p1)
{
	if (this->x < p1.x && this->y < p1.y && this->th < p1.th)
		return true;
	else
		return false;
}
void Pose::getPose(float& _x, float& _y, float& _th)
{
	_x = x; _y = y; _th = th;
	//cout << "\nnew pose x = " << _x << " y = " << _y << " th = " << _th << endl;
}
void Pose::setPose(float _x, float _y, float _th) { x = _x;  y = _y;  th = _th; }
float Pose::findDistanceTo(Pose pos)
{
	float dis = 0;
	dis = sqrt(pow(this->x - pos.x, 2) + pow(this->y - pos.y, 2));
	return dis;
}
float Pose::findAngleTo(Pose pos)
{
	float ang;
	ang = abs(this->th - pos.th);
	return ang;
}

